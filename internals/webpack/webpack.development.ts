import path from 'path';
import Dotenv from 'dotenv-webpack';
import webpack from 'webpack';

import baseConfig from './webpack.base';

const dotenv = new Dotenv({
  path: path.join(process.cwd(), 'internals/environment/.env.development'),
});

const config: webpack.Configuration = {
  ...baseConfig,
  devServer: {
    clientLogLevel: 'debug',
    historyApiFallback: true,
  },
  mode: 'development',
  output: {
    publicPath: '/',
  },
  plugins: [dotenv, ...baseConfig.plugins],
};

console.log('webpack development');
console.log(config);

export default config;
