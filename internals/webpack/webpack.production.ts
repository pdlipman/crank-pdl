import path from 'path';
import Dotenv from 'dotenv-webpack';
import webpack from 'webpack';

import baseConfig from './webpack.base';

const dotenv = new Dotenv({
  path: path.join(process.cwd(), 'internals/environment/.env.production'),
});

const config: webpack.Configuration = {
  ...baseConfig,
  mode: 'production',
  output: {
    filename: 'bundle.js',
    path: path.join(process.cwd(), 'dist'),
    publicPath: '/crank-pdl/',
  },
  plugins: [dotenv, ...baseConfig.plugins],
};

export default config;
