import HtmlWebPackPlugin from 'html-webpack-plugin';
import path from 'path';
import webpack from 'webpack';

const htmlPlugin = new HtmlWebPackPlugin({
  inject: true,
  template: path.join(process.cwd(), 'src/index.html'),
});

const baseConfig: webpack.Configuration = {
  entry: [path.join(process.cwd(), 'src/index.tsx')],
  module: {
    rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }],
  },
  plugins: [htmlPlugin],
  resolve: {
    alias: {
      components: path.join(process.cwd(), './src/components/'),
    },
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ['.ts', '.tsx', '.js', '.json'],
  },
};

export default baseConfig;
