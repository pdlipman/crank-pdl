// const componentExists = require('../utils/componentExists');

const componentGenerator = {
  description: 'Create a component',
  // User input prompts provided as arguments to the template
  prompts: [
    {
      // Raw text input
      type: 'input',
      // Variable name for this input
      name: 'name',
      // Prompt to display on command line
      message: 'What is your component name?',
      // validate: (value) => {
      //   if (/.+/.test(value)) {
      //     return componentExists(value)
      //       ? 'A component or container with this name already exists'
      //       : true;
      //   }

      //   return 'The name is required';
      // },
    },
    {
      type: 'confirm',
      name: 'wantTests',
      default: true,
      message: 'Add tests?',
    },
  ],
  actions: (data: any) => {
    const actions = [
      {
        // Add a new file
        type: 'add',
        // Path for the new file
        path: '../../src/components/{{dashCase name}}/index.tsx',
        // Handlebars template used to generate content of new file
        templateFile: './component/index.tsx.hbs',
        abortOnFail: true,
      },
    ];

    if (data.wantTests) {
      actions.push({
        type: 'add',
        path: '../../src/components/{{dashCase name}}/tests/index.spec.tsx',
        templateFile: './component/index.spec.tsx.hbs',
        abortOnFail: true,
      });
    }

    actions.push({
      type: 'prettify',
      path: '/src/components/',
      templateFile: '',
      abortOnFail: true,
    });

    return actions;
  },
};

export default componentGenerator;
