import { execSync } from 'child_process';
import path from 'path';
import { NodePlopAPI } from 'plop';
import componentGenerator from './component/index';

export default function (plop: NodePlopAPI): void {
  // plop generator code
  plop.setGenerator('component', componentGenerator);

  plop.setActionType('prettify', (answers: any, config: any) => {
    const folderPath = `${path.join(
      __dirname,
      '/../../',
      config.path,
      plop.getHelper('dashCase')(answers.name),
      '**',
      '**.{ts,tsx}',
    )}`;

    execSync(`yarn prettier --write "${folderPath}"`);
    return folderPath;
  });
}
