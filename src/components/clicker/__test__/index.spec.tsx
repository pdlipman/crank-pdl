/** @jsx createElement */
import { createElement } from '@bikeshaving/crank';
import { render } from 'crank-testing-library';
import userEvent from '@testing-library/user-event';

import { Clicker } from '../';

test('renders name', () => {
  const { getByTestId, getByText } = render(<Clicker />);
  expect(getByText('The button has been clicked 0 times.')).toBeTruthy();
  const button = getByTestId('clicker-button');
  userEvent.click(button);
  expect(getByText('The button has been clicked 1 time.')).toBeTruthy();
});
