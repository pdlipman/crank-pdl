/** @jsx createElement */
import { createElement } from '@bikeshaving/crank';

export function* Clicker() {
  let count = 0;
  const handleClick = () => {
    count++;
    this.refresh();
  };

  while (true) {
    yield (
      <div>
        The button has been clicked {count} {count === 1 ? 'time' : 'times'}.
        <button data-testid="clicker-button" onclick={handleClick}>
          Click me
        </button>
      </div>
    );
  }
}
