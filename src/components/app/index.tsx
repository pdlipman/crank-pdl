/** @jsx createElement */
import { createElement, Element } from '@bikeshaving/crank';

import { Clicker } from '../clicker';

interface Props {
  title?: string;
}

export function App({ title = 'Hello World' }: Props): Element {
  return (
    <div data-testid="app-component">
      {title}
      <Clicker />
    </div>
  );
}
