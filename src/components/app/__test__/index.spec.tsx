/** @jsx createElement */
import { createElement } from '@bikeshaving/crank';
import { render } from 'crank-testing-library';
import { App } from '../';

test('renders name', () => {
  const { getByText } = render(<App />);

  expect(getByText('Hello World')).toBeTruthy();
});
