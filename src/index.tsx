/** @jsx createElement */
import { createElement } from '@bikeshaving/crank';
import { renderer } from '@bikeshaving/crank/dom';
import { App } from './components/app/index';

function Index() {
  return <App />;
}

renderer.render(<Index />, document.getElementById('root'));
